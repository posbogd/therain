﻿using Unity.Jobs;
using UnityEngine.Jobs;
using UnityEngine;

public struct DropJob : IJobParallelForTransform {
    public float moveSpeed;
    public float deltaTime;
    public float topBound;
    public float bottomBound;
    public void Execute(int index, TransformAccess transform)
    {
        Vector3 position = transform.position;
        position += moveSpeed * deltaTime * (transform.rotation * new Vector3(0,-1,0));
        if (position.y < bottomBound)
            position.y = topBound;
        transform.position = position;
    }
	
}
