﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;

public class RainManager : MonoBehaviour {
    public int dropsCount;
    public int dropsIncrement;
    public float moveSpeed;
    public float topBound;
    public float bottomBound;
    public float xLeftBound;
    public float xRightBound;
    public float zLeftBound;
    public float zRightBound;
    public GameObject rain;

    TransformAccessArray transforms;
    DropJob dropJob;
    JobHandle dropHandle;

    private void OnDisable()
    {
        dropHandle.Complete();
        transforms.Dispose();
    }
    void Start () {
        transforms = new TransformAccessArray(0,-1);
        AddDrops(dropsCount);
	}
    void AddDrops(int amount) {
        dropHandle.Complete();
        transforms.capacity = transforms.length + amount;
        for (int i = 0; i < amount; i++) {
            float xVal = Random.Range(xLeftBound, xRightBound);
            float zVal = Random.Range(zLeftBound, zRightBound);
            float yVal = Random.Range(-5f, 5f);
            Vector3 pos = new Vector3(xVal, topBound+yVal, zVal);
            Quaternion rot = Quaternion.Euler(Vector3.zero);

            var obj = Instantiate(Resources.Load("DropPrefab", typeof(GameObject)),pos,rot,rain.transform) as GameObject;
            transforms.Add(obj.transform);
        }   
        
    }
	// Update is called once per frame
	void Update () {
        dropHandle.Complete();
        if (Input.GetKeyDown("space"))
            AddDrops(dropsIncrement);
        dropJob = new DropJob()
        {
            moveSpeed = this.moveSpeed,
            topBound = this.topBound,
            bottomBound = this.bottomBound,
            deltaTime = Time.deltaTime
        };
        dropHandle = dropJob.Schedule(transforms);
        JobHandle.ScheduleBatchedJobs();

	}

}
